﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetAndGetText : MonoBehaviour
{
    public InputField text;
    public pc obj;
    public Transform lib,mit,ignite,phydept,chemdept,Eng,acer,idc,exmcell,othr;
    public void SetGet()
    {
        if (string.Equals(text.text, "PhysicsDepartment", System.StringComparison.OrdinalIgnoreCase))
        {
            Debug.Log("Haiii");
            
            obj.Relpoints(phydept);
            
        }
        else if (string.Equals(text.text, "Library", System.StringComparison.OrdinalIgnoreCase))
        {
            
            obj.Relpoints(lib);
            
        }
        else if (string.Equals(text.text, "physics lab", System.StringComparison.OrdinalIgnoreCase))
        {
            
            obj.Relpoints(phydept);

        }
        else if (string.Equals(text.text, "physics department", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(phydept);

        }
        else if (string.Equals(text.text, "physics laboratory", System.StringComparison.OrdinalIgnoreCase))
        {
            
            obj.Relpoints(phydept);

        }
        else if (string.Equals(text.text, "chemistry department", System.StringComparison.OrdinalIgnoreCase))
        {
            
            obj.Relpoints(chemdept);

        }
        else if (string.Equals(text.text, "chemistry lab", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(chemdept);

        }
        else if (string.Equals(text.text, "chemistry laboratory", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(chemdept);

        }
        else if (string.Equals(text.text, "English department", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(Eng);

        }
        else if (string.Equals(text.text, "EP Lab", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(Eng);

        }
        else if (string.Equals(text.text, "EngineeringPractices Lab", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(Eng);

        }
        else if (string.Equals(text.text, "Acer Lab", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(acer);

        }
        else if (string.Equals(text.text, "acerLab", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(acer);

        }
        else if (string.Equals(text.text, "idc cell", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(idc);

        }
        else if (string.Equals(text.text, "Dean cabin", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(idc);

        }
        else if (string.Equals(text.text, "exam cell", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(exmcell);

        }
        else if (string.Equals(text.text, "S&H cabin", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(exmcell);

        }
        else if (string.Equals(text.text, "Ignite startup accelerator", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(ignite);

        }
        else if (string.Equals(text.text, "Ignite center", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(ignite);

        }
        else if (string.Equals(text.text, "Ignite", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(ignite);

        }
        else if (string.Equals(text.text, "startup accelerator", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(ignite);

        }
        else if (string.Equals(text.text, "Mitshubishi", System.StringComparison.OrdinalIgnoreCase))
        {

            obj.Relpoints(mit);

        }
        else
        {
            
            obj.Relpoints(othr);
            

        }
    }
}
