﻿using UnityEngine;
using System.Collections;

public class CameraBehaviour : MonoBehaviour
{
    //if block declaration
    private const float Y_min = 0.06f;
    private const float Y_max = 50.0f;
    public Transform lookAt;
    public Transform camTransform;
    private Camera cam;
    private float distance = 10.0f;
    private float currentX = 0.0f;
    private float currentY = 0.0f;
    private float sensitivityX = 4.0f;
    private float sensitivityY = 1.0f;


    private Quaternion rotation;
    private Vector3 dir;



    private void Start()
    {
        camTransform = transform;
        cam = Camera.main;
    }
    private void Update()
    {
        currentX += Input.GetAxis("Mouse X");
        currentY += Input.GetAxis("Mouse Y");
        currentY = Mathf.Clamp(currentY, Y_min, Y_max);

    }
    private void LateUpdate()
    {
        if (Input.GetMouseButton(1))
        {
            dir = new Vector3(0, 0, -distance);
            rotation = Quaternion.Euler(currentY, currentX, 0);
            camTransform.position = lookAt.position + rotation * dir;
            camTransform.LookAt(lookAt.position);
        }
        else
        {
            camTransform.position = lookAt.position + rotation * dir;
            camTransform.LookAt(lookAt.position);


        }
    }

}